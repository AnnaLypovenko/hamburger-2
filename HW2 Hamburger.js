class Hamburger {
    constructor(size, stuffing) {

        try {
            if (!size) {
                throw new HamburgerException("Choose the correct size");
            }
            if (!stuffing) {
                throw  new HamburgerException("Stuffing is not correct");
            }
            if (size.type !== 'size') {
                throw new HamburgerException('Wrong size')
            }
            if (stuffing.type !== "stuffing") {
                throw new HamburgerException("Wrong stuffing type")
            }

            this.size = size;
            this.stuffing = stuffing;
            this.topping = [];

        } catch (error) {
            console.log(error.name, error.message)
        }
    }

    calculateCalories() {
        let toppingCalories = 0;
        this.topping.forEach(item => toppingCalories += item.calories);
        return this.size.calories + this.stuffing.calories + toppingCalories
    };

    calculatePrice() {
        let toppingPrice = 0;
        this.topping.forEach(item => toppingPrice += item.price);
        return this.size.price + this.stuffing.price + toppingPrice
    };

    getSize() {
        return this.size;
    };

    getToppings() {
        return this.topping;
    };

    getStuffing() {
        return this.stuffing;
    };

    addTopping(topping) {
        try {
            if (this.topping.includes(topping)) {
                throw new HamburgerException("topping already added")
            }

            this.topping.push(topping);

        } catch (error) {
            console.log(error.name, error.message)
        }
    };

    removeTopping(topping) {
        const findTopping = this.topping.find(item => item === topping);
        const index = this.topping.indexOf(findTopping);

        return this.topping.splice(index, 1);
    };
}

Hamburger.SIZE_SMALL = {
    price: 50,
    calories: 20,
    type: 'size'
};
Hamburger.SIZE_LARGE = {
    price: 100,
    calories: 40,
    type: 'size'
};
Hamburger.STUFFING_CHEESE = {
    price: 10,
    calories: 20,
    type: 'stuffing'
};
Hamburger.STUFFING_SALAD = {
    price: 20,
    calories: 5,
    type: 'stuffing'
};
Hamburger.STUFFING_POTATO = {
    price: 15,
    calories: 10,
    type: 'stuffing'
};
Hamburger.TOPPING_MAYO = {
    price: 5,
    calories: 20
};
Hamburger.TOPPING_SPICE = {
    price: 10,
    calories: 20
};

function HamburgerException(message) {
    this.message = message;
    this.name = 'Hamburger error is: ';
}

let a = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_SALAD);
let e = new Hamburger(Hamburger.STUFFING_CHEESE, Hamburger.SIZE_SMALL);
let b = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
console.log(a.calculateCalories() + ' hamb A');
console.log(a.calculatePrice() + ' hamb A');
console.log(a.getSize());
console.log(a.getStuffing());
console.log(a.getToppings());
console.log(a.getToppings());

a.addTopping(Hamburger.TOPPING_MAYO);
a.addTopping(Hamburger.TOPPING_MAYO);
a.addTopping(Hamburger.TOPPING_SPICE);
a.addTopping(Hamburger.TOPPING_SPICE);
console.log(a.getToppings());
a.removeTopping(Hamburger.TOPPING_MAYO);
console.log(a.getToppings());

